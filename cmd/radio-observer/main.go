package main

import (
	"context"
	"os"

	clockhouse "radio-observer/internal/adapters/clickhouse"
	listenersstorage "radio-observer/internal/adapters/clickhouse/listeners"
	livenezstorage "radio-observer/internal/adapters/clickhouse/livenez"
	myradio24 "radio-observer/internal/adapters/radio/myradio24"
	radioheart "radio-observer/internal/adapters/radio/radio_heart"
	"radio-observer/internal/observer"
	"radio-observer/internal/router"
	"radio-observer/internal/usecase/listeners"
	"radio-observer/internal/usecase/livenez"
)

func main() {
	ctx := context.Background()

	dsn := os.Getenv("DATABASE_URL")
	db := clockhouse.Init(ctx, dsn)

	listenersStorage := listenersstorage.NewListenersStorage(db)
	livenezStorage := livenezstorage.NewLivenezStorage(db)

	listenersUseCase := listeners.NewUseCase(listenersStorage)
	livenezUseCase := livenez.NewUseCase(livenezStorage)

	radioHeart := radioheart.NewRadioHeart()
	myRadio24 := myradio24.NewMyRadio24()

	go observer.StartListenersObserver(ctx, myRadio24, radioHeart, listenersUseCase)

	router.StartRouter(ctx, listenersStorage, livenezUseCase)
}
