CREATE TABLE radiocounter.measurements
(
    measure_dttm    DateTime CODEC(DoubleDelta),
    listeners_count UInt16  CODEC(Gorilla)
) ENGINE = MergeTree()
      ORDER BY measure_dttm;