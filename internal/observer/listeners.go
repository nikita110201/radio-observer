package observer

import (
	"context"
	"log"
	"radio-observer/internal/domain"
	radiointerface "radio-observer/internal/usecase/adapters/radio_interface"
	usecaseinterface "radio-observer/internal/usecase/adapters/usecase_interface"
	"time"
)

func StartListenersObserver(ctx context.Context, myRadio24 radiointerface.Radio, radioHeart radiointerface.Radio, listenersUseCase usecaseinterface.Listeners) {
	var insert = make(chan domain.UniqueListeners)

	go insertGorutines(ctx, &insert, listenersUseCase)

	count1, _ := myRadio24.GetListeners(ctx)
	count2, _ := radioHeart.GetListeners(ctx)
	var (
		count11, count22 int64
		err              error
	)
	for {
		TimeNow := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), time.Now().Hour(), 0, 0, 0, time.UTC)
		count11, err = myRadio24.GetListeners(ctx)
		if count11-count1 > 0 && err == nil {
			insert <- domain.UniqueListeners{Date: TimeNow, Count: count11 - count1}
		}
		if count11 > 0 {
			count1 = count11
		}

		count22, err = radioHeart.GetListeners(ctx)
		if count22-count2 > 0 && err == nil {
			insert <- domain.UniqueListeners{Date: TimeNow, Count: count11 - count1}
		}
		if count22 > 0 {
			count2 = count22
		}
	}
}

func insertGorutines(ctx context.Context, insert *chan (domain.UniqueListeners), listenersUseCase usecaseinterface.Listeners) {
	for {
		select {
		case elem := <-*insert:
			listeners := elem

			err := listenersUseCase.InsertUniqueListeners(ctx, &listeners)
			if err != nil {
				log.Print(err)
			}
		}
	}
}
