package domain

import "time"

type PeriodTime struct {
	Start time.Time
	End   time.Time
	Mod   PeriodMod
}

type PeriodMod int64

const (
	PeriodDay = PeriodMod(iota)
	PeriodWeek
	PeriodMonth
	PeriodYear
)
