package domain

import "time"

type UniqueListeners struct {
	Date  time.Time
	Count int64
}
