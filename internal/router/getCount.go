package router

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"radio-observer/internal/domain"
	"time"
)

type Request struct {
	Start string `json:"start"`
	Mod   int    `json:"mod"`
}

type Response struct {
	Status string                    `json:"status,omitempty"`
	Data   []*domain.UniqueListeners `json:"data"`
}

func (router Router) GetCount(ctx context.Context) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		request := &Request{}
		if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
			Respond(w, Message(false, "Invalid request"))
			return
		}

		startTime, err := time.Parse(time.RFC3339, request.Start)
		if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
			Respond(w, Message(false, "Invalid start time"))
			return
		}

		period := &domain.PeriodTime{
			Start: startTime,
			Mod:   domain.PeriodMod(request.Mod),
		}

		res, err := router.listenersUseCase.GetUniqueListeners(ctx, period)
		if err != nil {
			Respond(w, Message(false, err.Error()))
			return
		}

		msg := Response{"OK", res}

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(msg)
		if err != nil {
			log.Print(err)
		}
		w.Header().Add("Access-Control-Allow-Origin", "*")
	}
}
