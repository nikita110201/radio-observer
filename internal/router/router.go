package router

import (
	"context"
	"log"
	"net/http"
	"os"
	usecaseinterface "radio-observer/internal/usecase/adapters/usecase_interface"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type Router struct {
	router           *mux.Router
	listenersUseCase usecaseinterface.Listeners
	livenezUseCase   usecaseinterface.Livenez
}

func StartRouter(ctx context.Context, listenersUseCase usecaseinterface.Listeners, livenezUseCase usecaseinterface.Livenez) {
	r := &Router{
		router:           mux.NewRouter(),
		listenersUseCase: listenersUseCase,
		livenezUseCase:   livenezUseCase,
	}

	r.router.HandleFunc("/api/get", r.GetCount(ctx)).Methods("POST")
	r.router.HandleFunc("/healthz", r.healthz)
	r.router.HandleFunc("/livenez", r.livenez(ctx))
	r.router.HandleFunc("/readyz", r.readyz)

	log.Print("Starting...")
	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}

	srv := &http.Server{
		Addr:    ":" + port,
		Handler: cors.Default().Handler(r.router),
	}

	log.Print("The service is ready to listen and serve.")
	log.Fatal(srv.ListenAndServe())
}
