package router

import (
	"context"
	"log"
	"net/http"
)

func (router *Router) livenez(ctx context.Context) http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) {
		flag := router.livenezUseCase.ConnectionIsLive(ctx)
		if !flag {
			w.WriteHeader(http.StatusBadRequest)
			Respond(w, Message(false, "db cann't get"))
			log.Print("live probe negative")
			return
		}
		w.WriteHeader(http.StatusOK)
		Respond(w, Message(true, "good"))
		log.Print("live probe positive")
		return
	}
}
