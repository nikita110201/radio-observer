package storageinterface

import (
	"context"
	"radio-observer/internal/domain"
)

type Listeners interface {
	InsertUniqueListeners(ctx context.Context, UniqueListeners *domain.UniqueListeners) error
	GetUniqueListeners(ctx context.Context, Period *domain.PeriodTime) ([]*domain.UniqueListeners, error)
}
