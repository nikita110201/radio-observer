package usecaseinterface

import "context"

type Livenez interface {
	ConnectionIsLive(ctx context.Context) bool
}
