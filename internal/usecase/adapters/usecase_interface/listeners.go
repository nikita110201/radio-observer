package usecaseinterface

import (
	"context"
	"radio-observer/internal/domain"
)

type Listeners interface {
	GetUniqueListeners(ctx context.Context, period *domain.PeriodTime) ([]*domain.UniqueListeners, error)
	InsertUniqueListeners(ctx context.Context, UniqueListeners *domain.UniqueListeners) error
}
