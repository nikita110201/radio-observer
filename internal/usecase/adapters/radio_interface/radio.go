package radiointerface

import "context"

type Radio interface {
	GetListeners(ctx context.Context) (int64, error)
}
