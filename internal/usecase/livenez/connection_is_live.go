package livenez

import (
	"context"
)

func (uc *UseCase) ConnectionIsLive(ctx context.Context) bool {
	for i := 0; i < 4; i++ {
		flag := uc.livenezStorage.ConnectionIsLive(ctx)
		if flag {
			return true
		}
	}
	return false
}
