package livenez

import (
	storageinterface "radio-observer/internal/usecase/adapters/storage_interface"
)

type UseCase struct {
	livenezStorage storageinterface.Livenez
}

func NewUseCase(livenezStorage storageinterface.Livenez) *UseCase {
	return &UseCase{
		livenezStorage: livenezStorage,
	}
}
