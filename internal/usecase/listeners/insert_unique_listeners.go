package listeners

import (
	"context"
	"radio-observer/internal/domain"
)

func (uc UseCase) InsertUniqueListeners(ctx context.Context, UniqueListeners *domain.UniqueListeners) error {
	return uc.listenersStorage.InsertUniqueListeners(ctx, UniqueListeners)
}
