package listeners

import (
	"context"
	"errors"
	"radio-observer/internal/domain"
	"time"
)

func (uc *UseCase) GetUniqueListeners(ctx context.Context, period *domain.PeriodTime) ([]*domain.UniqueListeners, error) {
	switch period.Mod {
	case domain.PeriodDay:
		period.End = period.Start.Add(time.Hour * 23)
	case domain.PeriodWeek:
		if period.Start.Weekday() != time.Monday {
			return nil, errors.New("Invalid body of request(week)")
		}
		period.End = period.Start.AddDate(0, 0, 6).Add(time.Hour * 23)
	case domain.PeriodMonth:
		if period.Start.Day() != 1 {
			return nil, errors.New("Invalid body of request(month)")
		}
		period.End = period.Start.AddDate(0, 1, 0).Add(-1 * time.Hour)
	case domain.PeriodYear:
		if period.Start.Day() != 1 && period.Start.Month() != 1 {
			return nil, errors.New("Invalid body of request(year)")
		}
		period.End = period.Start.AddDate(1, 0, 0).Add(-1 * time.Hour)
	}

	return uc.listenersStorage.GetUniqueListeners(ctx, period)
}
