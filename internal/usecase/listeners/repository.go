package listeners

import (
	storageinterface "radio-observer/internal/usecase/adapters/storage_interface"
)

type UseCase struct {
	listenersStorage storageinterface.Listeners
}

func NewUseCase(listenersStorage storageinterface.Listeners) *UseCase {
	return &UseCase{
		listenersStorage: listenersStorage,
	}
}
