package myradio24

import (
	"net/http"
	"time"
)

type Radio struct {
	httpClient *http.Client
}

func NewMyRadio24() *Radio {
	return &Radio{
		httpClient: &http.Client{
			Timeout: time.Second * 1,
		},
	}
}
