package myradio24

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
)

const url = "http://myradio24.com/users/meganight/status.json"

func (radio *Radio) GetListeners(ctx context.Context) (int64, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}
	res, err := radio.httpClient.Do(req)
	if err != nil {
		return 0, err
	}

	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return 0, err
	}
	num, ok := result["listeners"]
	if ok == false {
		return 0, errors.New("empty result_map")
	}
	count, ok := num.(int64)
	if !ok {
		return 0, errors.New("cann't convert")
	}
	return count, nil
}
