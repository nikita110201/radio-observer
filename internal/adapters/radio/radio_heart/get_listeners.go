package radioheart

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
)

const url = "https://a4.radioheart.ru/api/json?userlogin=user8042&api=current_listeners"

func (radio *Radio) GetListeners(ctx context.Context) (int64, error) {
	req, _ := http.NewRequest("GET", url, nil)
	res, err := radio.httpClient.Do(req)
	if err != nil {
		return -1, err
	}
	var result map[string]string
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return 0, err
	}
	num, Err := result["listeners"]
	if Err == false {
		return 0, errors.New("empty result_map")
	}
	out, err := strconv.Atoi(num)
	if err != nil {
		return 0, err
	}
	return int64(out), nil
}
