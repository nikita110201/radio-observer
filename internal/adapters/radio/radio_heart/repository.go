package radioheart

import (
	"net/http"
	"time"
)

type Radio struct {
	httpClient *http.Client
}

func NewRadioHeart() *Radio {
	return &Radio{
		httpClient: &http.Client{
			Timeout: time.Second * 1,
		},
	}
}
