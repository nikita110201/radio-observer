package livenezstorage

import (
	"context"
	"time"
)

func (st Storage) ConnectionIsLive(ctx context.Context) bool {
	ctxTime, cancel := context.WithTimeout(ctx, 500*time.Millisecond)
	defer cancel()

	clickHousePingResult := make(chan error)

	go func() {
		clickHousePingResult <- st.db.Ping()
	}()

	select {
	case <-ctxTime.Done():
		return false
	case tmp := <-clickHousePingResult:
		if tmp != nil {
			return false
		}
		return true
	}
}
