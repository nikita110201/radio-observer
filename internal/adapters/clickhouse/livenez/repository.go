package livenezstorage

import (
	"database/sql"
)

type Storage struct {
	db *sql.DB
}

func NewLivenezStorage(db *sql.DB) *Storage {
	return &Storage{
		db: db,
	}
}
