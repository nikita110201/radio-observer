package listenersstorage

import (
	"context"
	"log"
	"radio-observer/internal/domain"
	"time"
)

func (st *Storage) InsertUniqueListeners(ctx context.Context, UniqueListeners *domain.UniqueListeners) error {
	_, err := st.db.ExecContext(ctx, "INSERT INTO radiocounter.measurements_1 VALUES (toDateTime(?, 'Europe/Moscow'), ?)", UniqueListeners.Date, UniqueListeners.Count)
	if err != nil {
		return err
	}
	moscowTime, _ := time.LoadLocation("Europe/Moscow")
	log.Print(UniqueListeners.Date.In(moscowTime).Format("2006-01-02 15:04:05"), " : ", UniqueListeners.Count)
	return nil
}
