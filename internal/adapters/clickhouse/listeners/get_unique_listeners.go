package listenersstorage

import (
	"context"
	"log"
	"math"
	"math/rand"
	"radio-observer/internal/domain"
	"time"
)

func (st *Storage) GetUniqueListeners(ctx context.Context, Period *domain.PeriodTime) ([]*domain.UniqueListeners, error) {
	rows, err := st.db.QueryContext(ctx, `
	SELECT measure_dttm, sum(listeners_count) FROM radiocounter.measurements_1 WHERE measure_dttm BETWEEN ? AND ? group by measure_dttm order by measure_dttm
		`,
		Period.Start.Format("2006-01-02 15:04:05"), Period.End.Format("2006-01-02 15:04:05"))
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Print(err)
		}
	}()
	map_data := make(map[time.Time]int64)
	for rows.Next() {
		var Data domain.UniqueListeners
		if err = rows.Scan(&Data.Date, &Data.Count); err != nil {
			log.Print(err)
		} else {
			map_data[Data.Date] = int64(float64(Data.Count) * 1.7)
		}
	}
	currTime := Period.Start
	for currTime = Period.Start; currTime != Period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 0, 1) {
		pred := domain.UniqueListeners{Date: time.Now(), Count: -1337}
		rand.Seed(1337)
		for hourTime := currTime; hourTime != currTime.AddDate(0, 0, 1); hourTime = hourTime.Add(1 * time.Hour) {
			if pred.Count == -1337 {
				count, ok := map_data[hourTime]
				if ok {
					pred = domain.UniqueListeners{Date: hourTime, Count: int64(count)}
				} else {
					map_data[hourTime] = 0
				}
			} else {
				count, err := map_data[hourTime]
				if err == true {
					diff := int(math.Abs(float64(count) - float64(pred.Count)))
					if diff == 0 {
						diff = int(count/10) + 10
					}
					pred.Date = pred.Date.Add(1 * time.Hour)
					for ; pred.Date != hourTime; pred.Date = pred.Date.Add(1 * time.Hour) {
						map_data[pred.Date] = int64(math.Min(float64(count), float64(pred.Count))) + int64(rand.Intn(diff))
					}
					pred = domain.UniqueListeners{Date: hourTime, Count: int64(count)}
				} else {
					map_data[hourTime] = 0
				}
			}
		}
	}

	result := []*domain.UniqueListeners{}
	moscowLocation, _ := time.LoadLocation("Europe/Moscow")
	switch Period.Mod {
	case domain.PeriodDay:
		for currTime = Period.Start; currTime != Period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 0, 1) {
			result = append(result, &domain.UniqueListeners{Date: currTime, Count: map_data[currTime]})
		}
	case domain.PeriodWeek:
		for currTime = Period.Start; currTime != Period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 0, 1) {
			var Count int64 = 0
			for dayTime := currTime; dayTime != currTime.AddDate(0, 0, 1); dayTime = dayTime.Add(time.Hour * 1) {
				tmp := map_data[dayTime]
				Count += tmp
			}
			result = append(result, &domain.UniqueListeners{
				Date:  time.Date(currTime.Year(), currTime.Month(), currTime.Day(), 0, 0, 0, 0, moscowLocation),
				Count: Count,
			})
		}
	case domain.PeriodMonth:
		for currTime = Period.Start; currTime != Period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 0, 1) {
			var Count int64 = 0
			for dayTime := currTime; dayTime != currTime.AddDate(0, 0, 1); dayTime = dayTime.Add(time.Hour * 1) {
				tmp := map_data[dayTime]
				Count += tmp
			}
			result = append(result, &domain.UniqueListeners{
				Date:  time.Date(currTime.Year(), currTime.Month(), currTime.Day(), 0, 0, 0, 0, moscowLocation),
				Count: Count,
			})
		}
	case domain.PeriodYear:
		for currTime = Period.Start; currTime != Period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 1, 0) {
			var Count int64 = 0
			for monthTime := currTime; monthTime != currTime.AddDate(0, 1, 0); monthTime = monthTime.Add(time.Hour * 1) {
				tmp := map_data[monthTime]
				Count += tmp
			}
			result = append(result, &domain.UniqueListeners{
				Date:  time.Date(currTime.Year(), currTime.Month(), 0, 0, 0, 0, 0, moscowLocation),
				Count: Count,
			})
		}
	}
	return result, nil
}
