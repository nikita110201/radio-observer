package listenersstorage

import (
	"database/sql"
)

type Storage struct {
	db *sql.DB
}

func NewListenersStorage(db *sql.DB) *Storage {
	return &Storage{
		db: db,
	}
}
