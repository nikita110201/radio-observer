package clockhouse

import (
	"context"
	"database/sql"
	"log"
)

func Init(_ context.Context, dsn string) *sql.DB {
	db, err := sql.Open("clickhouse", dsn) // *sql.DB
	if err != nil {
		log.Fatalf("failed to load driver: %v", err)
	}
	return db
}
